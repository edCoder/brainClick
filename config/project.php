<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Project Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'ipinfo_token' => env('IPINFO_TOKEN', ''),

    'google_map_api' => env('GOOGLE_MAP_API', ''),
];
