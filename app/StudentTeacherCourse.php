<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTeacherCourse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id',
        'teacher_course_id',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get the student.
     */
    public function student()
    {
        return $this->belongsTo(\App\Student::class);
    }

    /**
     * Get the teacherCourse.
     */
    public function teacherCourse()
    {
        return $this->belongsTo(\App\TeacherCourse::class);
    }
}
