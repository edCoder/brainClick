<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'year_of_experience',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get the user for the teacher.
     */
    public function user()
    {
        return $this->morphOne(\App\User::class, 'userable');
    }
}
