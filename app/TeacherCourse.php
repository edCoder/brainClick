<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherCourse extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id',
        'name',
        'description',
        'avatar',
        'avatar_url',
    ];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    // protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Get the teacher.
     */
    public function teacher()
    {
        return $this->belongsTo(\App\Teacher::class);
    }
}
