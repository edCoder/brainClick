<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

use Carbon\Carbon;

if (! function_exists('carbonCreateDateTime')) {
    function carbonCreateDateTime($format, $date, $response_format = null, $day_position = null)
    {
        if (! $response_format) {
            $response_format = 'd/m/Y H:i:s';
        }
        try {
            if ($day_position) {
                if ($day_position == 'start') {
                    return Carbon::createFromFormat($format, $date)
                            ->startOfDay()
                            ->format($response_format);
                }
                if ($day_position == 'end') {
                    return Carbon::createFromFormat($format, $date)
                            ->endOfDay()
                            ->format($response_format);
                }
            }

            return Carbon::createFromFormat($format, $date)->format($response_format);
        } catch (Exception $e) {
            return null;
        }
    }
}

if (! function_exists('createFlashMessage')) {
    function createFlashMessage($message_type, $message, $clear_previous_flash = true)
    {
        if ($clear_previous_flash) {
            session()->flash('message_type', $message_type);
            session()->flash('message', $message);
        }
    }
}

if (! function_exists('checkObject')) {
    function checkObject($collection, $objects)
    {
        if (isset($collection)) {
            $current_object = $collection;
            foreach ($objects as $key => $object) {
                $current_object = $current_object->$object;
                if (! isset($current_object)) {
                    return null;
                }
            }

            return $current_object;
        }

        return null;
    }
}

if (! function_exists('makeAvatarUrl')) {
    function makeAvatarUrl($avatar, $type = 'image')
    {
        if($avatar) return url(str_replace('public', 'storage', $avatar));

        if($type == 'image') return url('images/no-image.png');
        
        return '';
    }
}

if (! function_exists('getClientIP')) {
    function getClientIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }
}

if (! function_exists('getBrowser')) {
    function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
       
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $bname = 'Apple Safari';
            $ub = "Safari";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $bname = 'Opera';
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $bname = 'Netscape';
            $ub = "Netscape";
        }
       
        // finally get the correct version number
        $known = ['Version', $ub, 'other'];
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
       
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }
       
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        $device_type_check = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $u_agent);
        if($device_type_check) {
            $device_type = 'mobile';
        } else {
            $device_type = 'computer';
        }
       
        return [
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern,
            'device_type'    => $device_type,
        ];
    }
}

if (! function_exists('apiResponse')) {
    function apiResponse($api_identifier, $status, $message, $data = null)
    {
        $api_response = [
            'metadata' => [
                'api_identifier' => $api_identifier,
                'status' => $status,
                'message' => $message,
            ],
            'data' => $data
        ];

        return response()->json($api_response, $status);
    }
}

if (! function_exists('makeAvatarFileName')) {
    function makeAvatarFileName($type, $id, $extension)
    {
        $filename = $type . '_' . $id . '_' . Str::uuid() . '.' . $extension;

        return $filename;
    }
}
