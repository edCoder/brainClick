<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Console\Commands;

use Mail;
use App\Mail\SendStudentMail;
use Illuminate\Console\Command;

class StudentEmailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'studentEmailCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'studentEmailCommand';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $students =
            \DB::table('students')
                ->join('users', function ($join) {
                    $join->on('users.userable_id', '=', 'students.id');
                    $join->where('users.userable_type', 'student');
                })
                ->select('students.id', 'students.name', 'users.email')
                ->get();

        foreach ($students as $student) {
            $student_teacher_courses = 
                \DB::table('student_teacher_courses')
                    ->where('student_teacher_courses.student_id', $student->id)
                    ->join('teacher_courses', function ($join) {
                        $join->on('teacher_courses.id', '=', 'student_teacher_courses.teacher_course_id');
                        $join->join('teachers', function ($join_2) {
                            $join_2->on('teachers.id', '=', 'teacher_courses.teacher_id');
                        });
                    })
                    ->select('student_teacher_courses.id', 'teacher_courses.name as course_name', 'teacher_courses.description as course_description', 'teacher_courses.avatar_url as course_avatar_url', 'teachers.name as teacher_name', 'teachers.year_of_experience as teacher_year_of_experience')
                    ->get();

            Mail::to($student->email)->send(new SendStudentMail($student, $student_teacher_courses));
        }
    }
}
