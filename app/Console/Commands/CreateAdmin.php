<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Console\Commands;

use App\Admin;
use App\User;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $header = ['Email', 'Username', 'Password'];

        $admin = User::where('userable_type', 'admin')->first();

        if ($admin) {
            $this->error('Admin already exists!');

            $body = [[$admin->email, $admin->username, '******']];
        } else {
            $admin = Admin::create([
                'first_name' => 'Super',
                'last_name' => 'Admin',
            ]);

            $password = $this->ask('Please enter superadmin password?');

            $user = User::create([
                'userable_id' => $admin->id,
                'userable_type' => 'admin',
                'email' => 'superadmin@admin.com',
                'username' => 'superadmin@admin.com',
                'userable_id' => $admin->id,
                'userable_type' => 'admin',
                'email' => 'superadmin@admin.com',
                'username' => 'superadmin@admin.com',
                'password' => bcrypt($password),
            ]);
            
            $this->info('Password updated!');

            $body = [['superadmin@admin.com', 'superadmin', $password]];
        }

        $this->table($header, $body);
    }
}
