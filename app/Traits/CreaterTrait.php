<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Traits;

trait CreaterTrait
{
    /**
     * Bootstrap trait.
     *
     * @return void
     */
    public static function bootCreaterTrait()
    {
        if (auth()->user()) {
            static::creating(function ($model) {
                $model->creator_id = \Auth::user()->id;
            });

            static::saving(function ($model) {
                if ($model->updated_at) {
                    $model->updater_id = \Auth::user()->id;
                }
            });
        }
    }

    /**
     * Get the creator.
     */
    public function creator()
    {
        return $this->belongsTo(\App\User::class, 'creator_id');
    }

    /**
     * Get the updater.
     */
    public function updater()
    {
        return $this->belongsTo(\App\User::class, 'updater_id');
    }
}
