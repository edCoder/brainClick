<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Traits;

trait AvatarUrlTrait
{
    /**
     * Get the employee name.
     *
     * @return string
     */
    public function getAvatarUrlAttribute()
    {
        if (\Storage::disk('local')->exists($this->avatar)) {
            return url(\Storage::disk('local')->url($this->avatar));
        }

        return asset('back_end/new/images/no-image.png');
    }
}
