<?php

namespace App\Http\Controllers\Api;

use Image;
use Carbon\Carbon;
use App\TeacherCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class TeacherApiController extends Controller
{
    /**
     * Handles Add Course
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCourse(Request $request)
    {
        $teacher_course = TeacherCourse::create([
            'teacher_id' => $request->user()->userable->id,
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $avatar = base64_encode($request->avatar);

        if ($avatar) {
            if (!file_exists('storage/teacher_course')) {
                mkdir('storage/teacher_course', 0777, true);
            }

            $file_name = 'storage/teacher_course/' . makeAvatarFileName('avatar', $teacher_course->id, $request->extension);
            $file_path = public_path($file_name);
            Image::make(base64_decode($avatar))->save($file_path);

            $teacher_course->update([
                'avatar' => \DB::raw("CONVERT(VARBINARY(MAX), '". $avatar ."')"),
                'avatar_url' => $file_name,
            ]);

            unset($teacher_course->avatar);
            $teacher_course->avatar_url = url($file_name);
        }

        $response = collect([
            'status' => 'success',
            'message' => 'teacher course added',
            'teacher_course' => $teacher_course,
        ]);

        return apiResponse('add_course', 200, 'Add Course', $response);
    }

    /**
     * Handles List Courses
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function course(Request $request)
    {
        $teacher_courses = 
            \DB::table('teacher_courses')
                ->join('teachers', function ($join) {
                    $join->on('teachers.id', '=', 'teacher_courses.teacher_id');
                })
                ->select('teacher_courses.id', 'teacher_courses.name', 'teacher_courses.description', 'teacher_courses.avatar_url', 'teachers.name as teacher_name', 'teachers.year_of_experience')
                ->paginate($request->per_page);

        return apiResponse('list_courses', 200, 'List Courses', $teacher_courses);
    }
}
