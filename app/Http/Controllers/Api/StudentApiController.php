<?php

namespace App\Http\Controllers\Api;

use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\StudentTeacherCourse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class StudentApiController extends Controller
{
    /**
     * Handles Subscribe Course
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribeCourse(Request $request)
    {
        $already_check = 
            \DB::table('student_teacher_courses')
                ->where('student_id', $request->user()->userable->id)
                ->where('teacher_course_id', $request->teacher_course_id)
                ->count();

        if($already_check) {
            $response = collect([
                'status' => 'error',
                'message' => 'already_subscribed',
                'student_teacher_course' => '',
            ]);

            return apiResponse('subscribe_course', 422, 'Subscribe Course', $response);
        }

        $student_teacher_course = StudentTeacherCourse::create([
            'student_id' => $request->user()->userable->id,
            'teacher_course_id' => $request->teacher_course_id,
        ]);

        $response = collect([
            'status' => 'success',
            'message' => 'student course subscribed',
            'student_teacher_course' => $student_teacher_course,
        ]);

        return apiResponse('subscribe_course', 200, 'Subscribe Course', $response);
    }

    /**
     * Handles List Courses
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function course(Request $request)
    {
        $student_teacher_courses = 
            \DB::table('student_teacher_courses')
                ->join('students', function ($join) {
                    $join->on('students.id', '=', 'student_teacher_courses.student_id');
                })
                ->join('teacher_courses', function ($join) {
                    $join->on('teacher_courses.id', '=', 'student_teacher_courses.teacher_course_id');
                    $join->join('teachers', function ($join_2) {
                        $join_2->on('teachers.id', '=', 'teacher_courses.teacher_id');
                    });
                })
                ->select('student_teacher_courses.id', 'students.name as student_name', 'teacher_courses.name as course_name', 'teacher_courses.description as course_description', 'teacher_courses.avatar_url as course_avatar_url', 'teachers.name as teacher_name', 'teachers.year_of_experience as teacher_year_of_experience')
                ->paginate($request->per_page);

        return apiResponse('list_courses', 200, 'List Courses', $student_teacher_courses);
    }
}
