<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LoginApiController extends Controller
{
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $access_token = auth()->user()->createToken('MyApp')->accessToken;

            return apiResponse('login', 200, 'login', [
                'status' => 'success',
                'access_token' => $access_token,
                'user' => auth()->user()->load('userable'),
            ]);
        }

        return apiResponse('login', 401, 'login', [
            'status' => 'error',
            'access_token' => '',
            'user' => '',
        ]);
    }
}
