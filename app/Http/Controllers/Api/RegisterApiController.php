<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Teacher;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class RegisterApiController extends Controller
{
    /**
     * Handles Teacher Registration
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function teacher(Request $request)
    {
        $already_check = 
            \DB::table('users')
                ->where('email', $request->email)
                ->count();

        if($already_check) {
            $response = collect([
                'status' => 'error',
                'message' => 'already_existed',
                'teacher' => '',
            ]);

            return apiResponse('register_teacher', 422, 'Register Teacher', $response);
        }

        $teacher = Teacher::create([
            'name' => $request->name,
            'year_of_experience' => (FLOAT)$request->year_of_experience,
        ]);

        $user = User::create([
            'userable_id' => $teacher->id,
            'userable_type' => 'teacher',
            'username' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $response = collect([
            'status' => 'success',
            'message' => 'teacher registered',
            'teacher' => $teacher->load('user'),
        ]);

        return apiResponse('register_teacher', 200, 'Register Teacher', $response);
    }

    /**
     * Handles Student Registration
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function student(Request $request)
    {
        $already_check = 
            \DB::table('users')
                ->where('email', $request->email)
                ->count();

        if($already_check) {
            $response = collect([
                'status' => 'error',
                'message' => 'already_existed',
                'student' => '',
            ]);

            return apiResponse('register_student', 422, 'Register Student', $response);
        }

        $student = Student::create([
            'name' => $request->name,
            'year_of_experience' => $request->year_of_experience,
        ]);

        $user = User::create([
            'userable_id' => $student->id,
            'userable_type' => 'student',
            'username' => $request->email,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $response = collect([
            'status' => 'success',
            'message' => 'student registered',
            'student' => $student->load('user'),
        ]);

        return apiResponse('register_student', 200, 'Register Student', $response);
    }
}
