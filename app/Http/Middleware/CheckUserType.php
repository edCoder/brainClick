<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Http\Middleware;

use Closure;

class CheckUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function handle($request, Closure $next, $user_type)
    {
        if ($user_type == $request->user()->userable_type) {
            return $next($request);
        }

        return apiResponse('unauthorized', 401, 'Unauthorized', []);
    }
}
