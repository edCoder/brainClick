<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('create', function ($response, $redirect) {
            createFlashMessage('error', 'Failed! Please try again');
            if ($response) {
                createFlashMessage('success', 'Record created');
            }

            return redirect($redirect);
        });

        Response::macro('update', function ($response, $redirect) {
            createFlashMessage('error', 'Failed! Please try again');
            if ($response) {
                createFlashMessage('success', 'Record updated');
            }

            return redirect($redirect);
        });

        Response::macro('delete', function ($response, $redirect) {
            createFlashMessage('error', 'Failed! Please try again', false);
            if ($response) {
                createFlashMessage('success', 'Record deleted');
            }

            return redirect($redirect);
        });

        Response::macro('head', function ($response, $redirect) {
            createFlashMessage('error', 'Failed! Please try again', false);
            if ($response) {
                createFlashMessage('success', 'Marked as head');
            }

            return redirect($redirect);
        });

        Response::macro('import', function ($response, $redirect) {
            createFlashMessage('error', 'Failed! Please try again', false);
            if ($response) {
                createFlashMessage('success', 'Imported successfully');
            }

            return redirect($redirect);
        });
    }
}
