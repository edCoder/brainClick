<?php

/**
 * ----- Brain Click -----
 *  
 * @copyright Brain Click
 *  
 * @link https://www.brainclickads.com
 *
 * @author Edwin Thomas <edwinthomas25@gmail.com>
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendStudentMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    protected $student, $student_teacher_courses;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($student, $student_teacher_courses)
    {
        $this->student = $student;
        $this->student_teacher_courses = $student_teacher_courses;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('STUDENT COURSE LIST')
            ->view('email.index', [
                'student' => $this->student,
                'student_teacher_courses' => $this->student_teacher_courses,
            ]);
    }
}
