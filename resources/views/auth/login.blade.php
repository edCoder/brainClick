<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Brain Click | Admin</title>
    </head>
    <body>
        <div class="container-scroller">
            <div class="container-fluid page-body-wrapper full-page-wrapper">
                <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                    <div class="row flex-grow">
                        <div class="col-lg-6 d-flex align-items-center justify-content-center">
                            <div class="auth-form-transparent text-left p-3">
                                <h4>Welcome back!</h4>
                                <h6 class="font-weight-light">Happy to see you again!</h6>
                                <form action="{{ route('login') }}" method="post" id="validate-form">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="email">
                                            Email
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend bg-transparent">
                                                <span class="input-group-text bg-transparent border-right-0">
                                                    <i class="mdi mdi-account-outline text-primary"></i>
                                                </span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="Email" 
                                                id="email" name="email" value="{{ old('email') }}" 
                                                autocomplete="off" required>
                                        </div>
                                        <label id="email-error" class="error" for="email"></label>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">
                                            Password
                                        </label>
                                        <div class="input-group">
                                            <div class="input-group-prepend bg-transparent">
                                                <span class="input-group-text bg-transparent border-right-0">
                                                    <i class="mdi mdi-account-outline text-primary"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Password" 
                                                id="password" name="password" value="{{ old('password') }}" 
                                                autocomplete="off" required>
                                        </div>
                                        <label id="password-error" class="error" for="password"></label>
                                    </div>
                                    <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                                        <div class="form-check">
                                            <label class="form-check-label text-muted">
                                                <input type="checkbox" class="form-check-input">
                                                Keep me signed in
                                            </label>
                                        </div>
                                    </div> -->
                                    <div class="my-3">
                                        <button class="btn btn-block btn-primary btn-lg font-weight-medium 
                                            auth-form-btn" type="submit">
                                            LOGIN
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 login-half-bg d-flex flex-row">
                            <p class="text-white font-weight-medium text-center flex-grow align-self-end">
                                Copyright © {{date('Y')}}
                                <a href="https://www.brainclickads.com/" target="_blank">
                                    Brain Click.
                                </a>
                                All rights reserved.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>