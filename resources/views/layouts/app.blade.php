<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Brain Click | Admin</title>

        @yield('styles')

    </head>
    <body>
        <div class="container-scroller">
            <div class="container-fluid page-body-wrapper">
                <div class="main-panel">
                    <div class="content-wrapper">
                        @yield('content')
                    </div>
                    <footer class="footer">
                        <div class="d-sm-flex justify-content-center justify-content-sm-between">
                            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">
                                Copyright © {{date('Y')}}
                                <a href="https://www.brainclickads.com/" target="_blank">
                                    Brain Click.
                                </a>
                                All rights reserved.
                            </span>
                        </div>
                    </footer>
                </div>
            </div>
        </div>

        @yield('scripts')

    </body>
</html>

