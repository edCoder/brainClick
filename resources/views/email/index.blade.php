<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Brain Click | Admin</title>
        <style type="text/css">
            table {
                width: 100%;
                border: 1px solid #000;
            }
            table thead tr th {
                background: #000;
                color: #fff;
                font-size: 18px;
                vertical-align: middle;
                text-align: center;
            }
            table tbody tr td {
                font-size: 14px;
                vertical-align: middle;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th colspan="5">
                        {{$student->name}}
                    </th>
                </tr>
                <tr>
                    <th width="20%">
                        COURSE IMAGE
                    </th>
                    <th width="20%">
                        COURSE NAME
                    </th>
                    <th width="20%">
                        COURSE DESCRIPTION
                    </th>
                    <th width="20%">
                        TEACHER NAME
                    </th>
                    <th width="20%">
                        TEACHER YEAR OF EXPERIENCE
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($student_teacher_courses as $student_teacher_course)
                    <tr>
                        <td>
                            <img height="75" src="{{makeAvatarUrl($student_teacher_course->course_avatar_url)}}">
                        </td>
                        <td>
                            {{$student_teacher_course->course_name}} 
                        </td>
                        <td>
                            {{$student_teacher_course->course_description}} 
                        </td>
                        <td>
                            {{$student_teacher_course->teacher_name}} 
                        </td>
                        <td>
                            {{$student_teacher_course->teacher_year_of_experience}} 
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>

