<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('creator_id')->nullable();
            $table->foreign('creator_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->unsignedInteger('updater_id')->nullable();
            $table->foreign('updater_id')
                        ->references('id')
                        ->on('users')
                        ->onDelete('no action');

            $table->unsignedBigInteger('teacher_id');
            $table->foreign('teacher_id')
                    ->references('id')
                    ->on('teachers')
                    ->onDelete('no action');
                        
            $table->string('name');
            $table->longText('description')->nullable();
            $table->binary('avatar')->nullable();
            $table->string('avatar_url')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_courses');
    }
}
