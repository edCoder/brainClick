Hi,

Gitlab repository: https://gitlab.com/edCoder/brainClick
API Base URL: https://brain-click.macnocomputers.com/

The command for sending student email: php artisan studentEmailCommand

I used Microsoft Azure Virtual Machine to host the project and used Task Scheduler to schedule the email.

For sending notifications to teachers, we can use kreait/laravel-firebase package with Firebase integration.  
Steps:

1. Create account in Firebase.
2. Install kreait/laravel-firebase package.
3. Download FIREBASE_CREDENTIALS json file.
4. Get FIREBASE_DATABASE_URL string.
5. Add FIREBASE_CREDENTIALS & FIREBASE_DATABASE_URL to .env file.
6. Sample code to send notification:

use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification; 

$messaging = app('firebase.messaging'); 

$topic_name = 'sample_topic';
$title = 'sample_title';
$body = 'sample_body'; 
$token  = 'sample_token';
$data = [
    'parameter_1' => 'value 1',
    'parameter_2' => 'value 2',
]; 

$message =
    CloudMessage::withTarget('topic', $topic_name)
                ->withTarget('token', $token)
                ->withNotification(Notification::create($title, $body))
                ->withData($data); 

$messaging->send($message);
