<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('register')->group(function () {
	Route::post('teacher', 'Api\RegisterApiController@teacher');
	Route::post('student', 'Api\RegisterApiController@student');
});

Route::prefix('login')->group(function () {
	Route::post('/', 'Api\LoginApiController@index');
});

Route::middleware(['auth:api', 'check_user_type:teacher'])->group(function () {
	Route::prefix('teacher')->group(function () {
    	Route::post('add-course', 'Api\TeacherApiController@addCourse');
    	Route::get('/course', 'Api\TeacherApiController@course');
    });
});

Route::middleware(['auth:api', 'check_user_type:student'])->group(function () {
	Route::prefix('student')->group(function () {
    	Route::post('subscribe-course', 'Api\StudentApiController@subscribeCourse');
    	Route::get('/course', 'Api\StudentApiController@course');
    });
});